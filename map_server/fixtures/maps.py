#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# which this script has hooks to.
#
# On that file, don't forget to add the necessary Django imports
# and take a look at how locate_object() and save_or_locate()
# are implemented here and expected to behave.
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script


IMPORT_HELPER_AVAILABLE = False
try:
    import import_helper
    IMPORT_HELPER_AVAILABLE = True
except ImportError:
    pass

import datetime
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType
import shutil
from os.path import abspath, dirname, join

def run():
    #initial imports

    def locate_object(original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        #You may change this function to do specific lookup for specific objects
        #
        #original_class class of the django orm's object that needs to be located
        #original_pk_name the primary key of original_class
        #the_class      parent class of original_class which contains obj_content
        #pk_name        the primary key of original_class
        #pk_value       value of the primary_key
        #obj_content    content of the object which was not exported.
        #
        #you should use obj_content to locate the object on the target db
        #
        #and example where original_class and the_class are different is
        #when original_class is Farmer and
        #the_class is Person. The table may refer to a Farmer but you will actually
        #need to locate Person in order to instantiate that Farmer
        #
        #example:
        #if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #    pk_name="name"
        #    pk_value=obj_content[pk_name]
        #if the_class == StaffGroup:
        #    pk_value=8


        if IMPORT_HELPER_AVAILABLE and hasattr(import_helper, "locate_object"):
            return import_helper.locate_object(original_class, original_pk_name, the_class, pk_name, pk_value, obj_content)

        search_data = { pk_name: pk_value }
        the_obj =the_class.objects.get(**search_data)
        return the_obj

    def save_or_locate(the_obj):
        if IMPORT_HELPER_AVAILABLE and hasattr(import_helper, "save_or_locate"):
            the_obj = import_helper.save_or_locate(the_obj)
        else:
            the_obj.save()
        return the_obj



    from map_server.models import Map

    if not Map.objects.filter(name='Bees'):

        from map_server.models import LayerDataset

        map_server_layerdataset_1 = LayerDataset()
        map_server_layerdataset_1.name = u'Fusion Table Example'
        map_server_layerdataset_1.source_location = u'1fplsWBhWBStTw0Ho_HswfbXg12LjHQDUSl7uSKc'
        map_server_layerdataset_1.source_type = u'fusion'
        map_server_layerdataset_1.render_location = u'local'
        map_server_layerdataset_1.latitude_field = u'lat'
        map_server_layerdataset_1.longitude_field = u'lng'
        map_server_layerdataset_1.decimal_precision = 3
        map_server_layerdataset_1.field_override = u''
        map_server_layerdataset_1.fusion_styles = u''
        map_server_layerdataset_1.popup_html = u'<h1>{{ venue }}</h1>'
        map_server_layerdataset_1.parser = u''
        map_server_layerdataset_1.max_result_count = None
        map_server_layerdataset_1 = save_or_locate(map_server_layerdataset_1)

        map_server_layerdataset_2 = LayerDataset()
        map_server_layerdataset_2.name = u'JSON Example'
        map_server_layerdataset_2.source_location = u'/maps/feeds/upcoming-events.json'
        map_server_layerdataset_2.source_type = u'json'
        map_server_layerdataset_2.render_location = u'local'
        map_server_layerdataset_2.latitude_field = u'lat'
        map_server_layerdataset_2.longitude_field = u'lng'
        map_server_layerdataset_2.decimal_precision = 3
        map_server_layerdataset_2.field_override = u''
        map_server_layerdataset_2.fusion_styles = u''
        map_server_layerdataset_2.popup_html = u'<h1>Event</h1><p>{{ title }}</p><dl><dt>Date: </dt><dd>{{ date }}</dd><br><dt>Time: </dt><dd>{{ time }}</dd></dl><a href="http://www.foe.co.uk/what_we_do/bee_cause_events_35047.html" target="_blank">Find out more &raquo;</a>'
        map_server_layerdataset_2.parser = u''
        map_server_layerdataset_2.max_result_count = None
        map_server_layerdataset_2 = save_or_locate(map_server_layerdataset_2)

        map_server_layerdataset_3 = LayerDataset()
        map_server_layerdataset_3.name = u'XML Example'
        map_server_layerdataset_3.source_location = u'/maps/feeds/lgs.xml'
        map_server_layerdataset_3.source_type = u'xml'
        map_server_layerdataset_3.render_location = u'local'
        map_server_layerdataset_3.latitude_field = u'latitude'
        map_server_layerdataset_3.longitude_field = u'longitude'
        map_server_layerdataset_3.decimal_precision = 3
        map_server_layerdataset_3.field_override = u''
        map_server_layerdataset_3.fusion_styles = u''
        map_server_layerdataset_3.popup_html = u'<h1>{{ title }}</h1><a href="{{ link }}" target="_blank">Go to Website &raquo;</a>'
        map_server_layerdataset_3.parser = u'function(json)\n{   var objects = [];\n\tfor(var i=0; i<json.channel.item.length; i++)\n\t{   objects.push(json.channel.item[i]);\n\t}\n\treturn objects;\n}'
        map_server_layerdataset_3.max_result_count = None
        map_server_layerdataset_3 = save_or_locate(map_server_layerdataset_3)


        #Processing model: MarkerIcon

        from map_server.models import MarkerIcon

        map_server_markericon_1 = MarkerIcon()
        map_server_markericon_1.name = u'Default'
        map_server_markericon_1.icon = u'markers/default.png'
        map_server_markericon_1 = save_or_locate(map_server_markericon_1)

        map_server_markericon_2 = MarkerIcon()
        map_server_markericon_2.name = u'Flower Blue 24px'
        map_server_markericon_2.icon = u'markers/flowerblue-24px.png'
        map_server_markericon_2 = save_or_locate(map_server_markericon_2)

        map_server_markericon_3 = MarkerIcon()
        map_server_markericon_3.name = u'Flower Green 24px'
        map_server_markericon_3.icon = u'markers/flowergreen-24px.png'
        map_server_markericon_3 = save_or_locate(map_server_markericon_3)

        map_server_markericon_4 = MarkerIcon()
        map_server_markericon_4.name = u'Theatre'
        map_server_markericon_4.icon = u'markers/theatre.png'
        map_server_markericon_4 = save_or_locate(map_server_markericon_4)

        map_server_markericon_5 = MarkerIcon()
        map_server_markericon_5.name = u'Theatre Cluster'
        map_server_markericon_5.icon = u'markers/theatre-cluster.png'
        map_server_markericon_5 = save_or_locate(map_server_markericon_5)

        map_server_markericon_6 = MarkerIcon()
        map_server_markericon_6.name = u'Flower Red 24px'
        map_server_markericon_6.icon = u'markers/flowerred-24px.png'
        map_server_markericon_6 = save_or_locate(map_server_markericon_6)

        map_server_markericon_7 = MarkerIcon()
        map_server_markericon_7.name = u'Flower Yellow 24px'
        map_server_markericon_7.icon = u'markers/floweryellow-24px.png'
        map_server_markericon_7 = save_or_locate(map_server_markericon_7)

        map_server_markericon_8 = MarkerIcon()
        map_server_markericon_8.name = u'Flower All 24px'
        map_server_markericon_8.icon = u'markers/flowersall-24px.png'
        map_server_markericon_8 = save_or_locate(map_server_markericon_8)

        map_server_markericon_9 = MarkerIcon()
        map_server_markericon_9.name = u'User'
        map_server_markericon_9.icon = u'markers/user.png'
        map_server_markericon_9 = save_or_locate(map_server_markericon_9)

        map_server_markericon_10 = MarkerIcon()
        map_server_markericon_10.name = u'User Cluster'
        map_server_markericon_10.icon = u'markers/user-cluster.png'
        map_server_markericon_10 = save_or_locate(map_server_markericon_10)

        map_server_markericon_11 = MarkerIcon()
        map_server_markericon_11.name = u'Actions'
        map_server_markericon_11.icon = u'markers/actions.png'
        map_server_markericon_11 = save_or_locate(map_server_markericon_11)

        map_server_markericon_12 = MarkerIcon()
        map_server_markericon_12.name = u'Actions Cluster'
        map_server_markericon_12.icon = u'markers/actions-cluster.png'
        map_server_markericon_12 = save_or_locate(map_server_markericon_12)

        map_server_markericon_13 = MarkerIcon()
        map_server_markericon_13.name = u'Events'
        map_server_markericon_13.icon = u'markers/events.png'
        map_server_markericon_13 = save_or_locate(map_server_markericon_13)

        map_server_markericon_14 = MarkerIcon()
        map_server_markericon_14.name = u'Events Cluster'
        map_server_markericon_14.icon = u'markers/events-cluster.png'
        map_server_markericon_14 = save_or_locate(map_server_markericon_14)

        map_server_markericon_15 = MarkerIcon()
        map_server_markericon_15.name = u'Bee Worlds'
        map_server_markericon_15.icon = u'markers/beeworlds.png'
        map_server_markericon_15 = save_or_locate(map_server_markericon_15)

        map_server_markericon_16 = MarkerIcon()
        map_server_markericon_16.name = u'Bee Worlds Cluster'
        map_server_markericon_16.icon = u'markers/beeworlds-cluster.png'
        map_server_markericon_16 = save_or_locate(map_server_markericon_16)

        map_server_markericon_17 = MarkerIcon()
        map_server_markericon_17.name = u'Constituencies Old'
        map_server_markericon_17.icon = u'markers/constituencies.png'
        map_server_markericon_17 = save_or_locate(map_server_markericon_17)

        map_server_markericon_18 = MarkerIcon()
        map_server_markericon_18.name = u'Local Groups'
        map_server_markericon_18.icon = u'markers/localGroups_2.png'
        map_server_markericon_18 = save_or_locate(map_server_markericon_18)

        map_server_markericon_19 = MarkerIcon()
        map_server_markericon_19.name = u'Community Energy Projects'
        map_server_markericon_19.icon = u'markers/communityEnergy.png'
        map_server_markericon_19 = save_or_locate(map_server_markericon_19)

        map_server_markericon_20 = MarkerIcon()
        map_server_markericon_20.name = u'Green Businesses'
        map_server_markericon_20.icon = u'markers/greenBusinesses.png'
        map_server_markericon_20 = save_or_locate(map_server_markericon_20)

        map_server_markericon_21 = MarkerIcon()
        map_server_markericon_21.name = u'Allies'
        map_server_markericon_21.icon = u'markers/allies_1.png'
        map_server_markericon_21 = save_or_locate(map_server_markericon_21)

        map_server_markericon_22 = MarkerIcon()
        map_server_markericon_22.name = u'Renewable Energy'
        map_server_markericon_22.icon = u'markers/renewableEnergy.png'
        map_server_markericon_22 = save_or_locate(map_server_markericon_22)

        map_server_markericon_23 = MarkerIcon()
        map_server_markericon_23.name = u'Local Groups Cluster'
        map_server_markericon_23.icon = u'markers/localGroups-cluster.png'
        map_server_markericon_23 = save_or_locate(map_server_markericon_23)

        map_server_markericon_24 = MarkerIcon()
        map_server_markericon_24.name = u'Green Businesses Cluster'
        map_server_markericon_24.icon = u'markers/greenBusinesses-cluster.png'
        map_server_markericon_24 = save_or_locate(map_server_markericon_24)

        map_server_markericon_25 = MarkerIcon()
        map_server_markericon_25.name = u'Renewable Energy Cluster'
        map_server_markericon_25.icon = u'markers/renewableEnergy-cluster.png'
        map_server_markericon_25 = save_or_locate(map_server_markericon_25)

        map_server_markericon_26 = MarkerIcon()
        map_server_markericon_26.name = u'Community Energy Cluster'
        map_server_markericon_26.icon = u'markers/communityEnergy-cluster.png'
        map_server_markericon_26 = save_or_locate(map_server_markericon_26)

        map_server_markericon_27 = MarkerIcon()
        map_server_markericon_27.name = u'Allies Cluster'
        map_server_markericon_27.icon = u'markers/allies-cluster.png'
        map_server_markericon_27 = save_or_locate(map_server_markericon_27)

        map_server_markericon_28 = MarkerIcon()
        map_server_markericon_28.name = u'Case Studies'
        map_server_markericon_28.icon = u'markers/casestudies.png'
        map_server_markericon_28 = save_or_locate(map_server_markericon_28)

        map_server_markericon_29 = MarkerIcon()
        map_server_markericon_29.name = u'Case Studies Cluster'
        map_server_markericon_29.icon = u'markers/casestudies-cluster.png'
        map_server_markericon_29 = save_or_locate(map_server_markericon_29)

        #Processing model: Layer

        from map_server.models import Layer

        map_server_layer_1 = Layer()
        map_server_layer_1.name = u'Bee Savers'
        map_server_layer_1.visible = True
        map_server_layer_1.legend_icon = map_server_markericon_8
        map_server_layer_1.should_cluster = False
        map_server_layer_1.cluster_icon = map_server_markericon_1
        map_server_layer_1.cluster_text_color = u'000000'
        map_server_layer_1.z_index = 0
        map_server_layer_1 = save_or_locate(map_server_layer_1)

        map_server_layer_1.datasets.add(map_server_layerdataset_1)
        map_server_layer_1.marker_icons.add(map_server_markericon_2)
        map_server_layer_1.marker_icons.add(map_server_markericon_3)
        map_server_layer_1.marker_icons.add(map_server_markericon_6)
        map_server_layer_1.marker_icons.add(map_server_markericon_7)

        map_server_layer_2 = Layer()
        map_server_layer_2.name = u'Theatre'
        map_server_layer_2.visible = False
        map_server_layer_2.legend_icon = map_server_markericon_4
        map_server_layer_2.should_cluster = True
        map_server_layer_2.cluster_icon = map_server_markericon_5
        map_server_layer_2.cluster_text_color = u'452743'
        map_server_layer_2.z_index = 0
        map_server_layer_2 = save_or_locate(map_server_layer_2)

        map_server_layer_2.datasets.add(map_server_layerdataset_2)
        map_server_layer_2.marker_icons.add(map_server_markericon_4)

        map_server_layer_3 = Layer()
        map_server_layer_3.name = u'Bee Petitions'
        map_server_layer_3.visible = False
        map_server_layer_3.legend_icon = map_server_markericon_11
        map_server_layer_3.should_cluster = True
        map_server_layer_3.cluster_icon = map_server_markericon_12
        map_server_layer_3.cluster_text_color = u'bb0b4d'
        map_server_layer_3.z_index = 0
        map_server_layer_3 = save_or_locate(map_server_layer_3)

        map_server_layer_3.datasets.add(map_server_layerdataset_3)
        map_server_layer_3.marker_icons.add(map_server_markericon_11)

        #Processing model: Map

        from map_server.models import Map

        map_server_map_1 = Map()
        map_server_map_1.name = u'Example Map'
        map_server_map_1.center_latitude = Decimal('54.7')
        map_server_map_1.center_longitude = Decimal('-2')
        map_server_map_1.tile_set = u'google.maps.MapTypeId.TERRAIN'
        map_server_map_1.specific_js = u'// Init\nfoeMap.appendToLegend($(\'#add_your_pin_container\'))\n$.cookie.json = true;\nreviveSubmissionCookie(); // Load up submissions saved in cookies\n\n$(\'#add_your_pin_button\').click(function()\n{   foeMap.minimizeLegend();\n    $(\'#add_your_pin_form\').show();\n    foeMap.trackEvent(\'Add Pin Button Clicked\');\n});\n\n$(\'.closeForm\').click(function()\n{   $(\'#add_your_pin_form\').hide();\n    $(\'#form_body\').css(\'visibility\', \'visible\');\n    $(\'#form_confirmation\').css(\'visibility\', \'hidden\');\n    foeMap.maximizeLegend();\n});\n\n$(\'#id_postcode\').focusout(function()   // Hack in a quick geocode on postcode blur\n{   foeMap.codeAddress($(\'#id_postcode\').val(), function(location)\n    {   console.log(\'coded: \' + location);\n        $(\'#id_latitude\').val(location.lat());\n        $(\'#id_longitude\').val(location.lng());\n    });\n});\n\n$(\'#add_your_pin_form\').iframePostForm(\n{   json: true,\n    complete: function (response)\n    {   if(response[\'id\'])\n            submissionSuccess(response);\n        else\n            console.log(\'Unexpected response from form submission: \' + response);\n    }\n});\n\nfunction submissionSuccess(response)\n{   console.log(\'Submission successful.  Id: \' + response[\'id\']);\n    $(\'#form_body\').css(\'visibility\', \'hidden\');  /* Visibility, not display, to maintain height */\n    $(\'#form_confirmation\').css(\'visibility\', \'visible\');\n    createSubmissionCookie($(\'#id_latitude\').val(), $(\'#id_longitude\').val(), response[\'photo\'], response[\'description\']);\n    if($.cookie(\'bee_submission_location\'))\n        foeMap.getLayer(\'Your Stories\').getDataset(\'Your Stories\').addPin($.cookie(\'bee_submission_location\'), true);\n    $(\'#add_your_pin_form\')[0].reset();\n    foeMap.trackEvent(\'Add Pin Submitted | Id: \' + response[\'id\']);\n}\n\nfunction createSubmissionCookie(lat, lng, photo, desc)\n{   $.cookie(\'bee_submission_location\', {\'lat\': lat, \'lng\':lng, \'photo\': photo,\'desc\': desc }, { expires: 30, path: \'/\' });\n}\n\nfunction reviveSubmissionCookie()\n{   if($.cookie(\'bee_submission_location\'))\n        foeMap.getLayer(\'Your Stories\').getDataset(\'Your Stories\').addPin($.cookie(\'bee_submission_location\'));\n}\n\n// Submit form validation\n$.validator.addMethod(\'ukpostcode\', function(value, element)\n{   return this.optional(element) || /(^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$)|(^xxx xxx$)/.test(value);\n}, "Enter UK Postcode (ie, N1 7JQ)");\n\nvar validator = $(\'#add_your_pin_form\').validate(\n{   rules:\n    {   firstname: \'required\',\n        lastname: \'required\',\n        email: { required: true, email: true },\n        postcode: { required: true, ukpostcode: true },\n        description: \'required\'\n    },\n    errorPlacement: function(error, element){ return; }\n});'
        map_server_map_1.specific_css = u'        <style type="text/css">\r\n            /* Map specific CSS */\r\n\t\t\t\r\n            .foeMapping #add_your_pin_container\r\n            {   padding: 0px 5px;\r\n                padding-bottom: 10px;\r\n                border-top: 1px solid #444;\r\n                box-shadow: inset 0px 1px #777;\r\n                display: none;\r\n            }\r\n\r\n            .foeMapping #legend #add_your_pin_container\r\n            {   display: block;\r\n            }\r\n\r\n            .foeMapping .divButton,\r\n            .foeMapping .divButton *\r\n            {   cursor: hand;\r\n                cursor: pointer;\r\n            }\r\n\r\n            .foeMapping .bigOrange\r\n            {   width: 100%;\r\n                height: 40px;\r\n                color: #FFF;\r\n                font-weight: 600;\r\n                border-radius: 3px;\r\n                text-shadow: #6a4500 0px 1px 1px;\r\n                background: #ff7f00;  /* Sampled from mockup */\r\n                background: rgba(255,127,0,0.9);\r\n                background-image: linear-gradient(bottom, rgb(239,127,0) 19%, rgb(255,132,0) 60%);\r\n                background-image: -o-linear-gradient(bottom, rgb(239,127,0) 19%, rgb(255,132,0) 60%);\r\n                background-image: -moz-linear-gradient(bottom, rgb(239,127,0) 19%, rgb(255,132,0) 60%);\r\n                background-image: -webkit-linear-gradient(bottom, rgb(239,127,0) 19%, rgb(255,132,0) 60%);\r\n                background-image: -ms-linear-gradient(bottom, rgb(239,127,0) 19%, rgb(255,132,0) 60%);\r\n                background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0.19, rgb(239,127,0)), color-stop(0.6, rgb(255,132,0)) );\r\n            }\r\n\r\n            .foeMapping .bigOrange > p\r\n            {   padding-top: 12px;\r\n            }\r\n\r\n            .foeMapping .bigOrange > img\r\n            {   float: left;\r\n                padding: 8px 10px 0 15px;\r\n                max-width: 28px;\r\n                /* height: 32px; */\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form\r\n            {\r\n                display: none;\r\n                position: absolute;\r\n                top: 10px;\r\n                left: 50%;\r\n                margin-left: -42.421%; /* -244 / 577 */\r\n                max-width: 533px;\r\n\t\t\t\twidth: 72%; /* ?? / 577 */\r\n                padding: 20px 35px;\r\n                padding-top: 5px;\r\n                /* Refactor out */\r\n                border-radius: 9px;\r\n                border: 1px solid #444;\r\n                background: #505060;\r\n                background: rgba(80,80,96,0.8);\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form #close_button\r\n            {   position: absolute;\r\n                top: 18px;\r\n                right: 18px;\r\n                height: 25px;\r\n                width: 25px;\r\n                background-color: #dbdcdd;\r\n                border-radius: 20px;\r\n                font-weight: bold;\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form #close_button p\r\n            {   color: rgba(80, 80, 96, 0.8);\r\n\t\t\t\tfont-size: 11px;\r\n\t\t\t\ttext-align: center;\r\n\t\t\t\tmargin: 0;\r\n\t\t\t\tline-height: 25px;\r\n\t\t\t\tfont-weight: bold;\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form p,\r\n            .foeMapping #add_your_pin_form label\r\n            {   font-weight: 500;\r\n                color: white;\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form p,\r\n            .foeMapping #add_your_pin_form label,\r\n            .foeMapping #add_your_pin_form input,\r\n            .foeMapping #add_your_pin_form textarea\r\n            {   display: block;\r\n                width: 100%;\r\n                box-sizing: border-box;\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form input,\r\n            .foeMapping #add_your_pin_form textarea\r\n            {   padding: 6px;\r\n                margin: 7px auto;\r\n                /* border-radius: 3px; */\r\n                border: 0;\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form label#dp\r\n            {   \r\n\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form label#dp #id_data_protection\r\n            {   width: auto;\r\n                display: inline;\r\n\t\t\t\tmargin: 0;\r\n            }\r\n\r\n            .foeMapping #add_your_pin_form button.bigOrange\r\n            {   position: relative;\r\n\t\t\t\tdisplay: block;\r\n                margin: 10px auto;\r\n                border-width: 0px;\r\n                font-size: 24px;\r\n\t\t\t\ttext-shadow: none;\r\n\t\t\t\twidth: auto;\r\n\t\t\t\tpadding: 7px 28px;\r\n\t\t\t\theight: auto;\r\n\t\t\t\tborder-radius: 9px;\r\n\t\t\t\tclear: both;\r\n            }\r\n\r\n            .foeMapping #form_confirmation\r\n            {   visibility: hidden;\r\n                position: absolute;\r\n                top: 93px;\r\n                text-align: center;\r\n                width: 300px;\r\n                left: 50%;\r\n                margin-left: -150px;\r\n            }\r\n\r\n            .foeMapping form#user_submission > table label > span\r\n            {   display: block;\r\n            }\r\n\r\n            .foeMapping form#user_submission > table td\r\n            {   padding-top: 15px;\r\n                vertical-align: top;\r\n            }\r\n\t\t\t\r\n            .foeMapping #map #legend\r\n            {   padding-top: 10px;\r\n            }\r\n\t\t\t#form_body h2,\r\n\t\t\t#form_confirmation h2 {\r\n                                color: #ffcd42;\r\n\t\t\t\tmargin-bottom: 5px;\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\t.textfields {\r\n\t\t\t\twidth: 60.50283105022831050228310502283%; /* 265px / 438px */\r\n\t\t\t\tfloat: left;\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\t.formimage {\r\n\t\t\t\ttext-align: center;\r\n\t\t\t\twidth: 39.497716894977168949771689497717%; /* 173 / 438 */\r\n\t\t\t\tfloat: right;\r\n\t\t\t\tmargin-top: 16px;\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\t#form_body input,\r\n\t\t\t#form_body textarea {\r\n\t\t\t\t-moz-box-sizing: border-box;\r\n\t\t\t\tbox-sizing: border-box\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\t.foeMapping #add_your_pin_form #id_photo {\r\n\t\t\t\tdisplay: inline-block;\r\n\t\t\t\twidth: auto;\r\n\t\t\t\tvertical-align: 50%;\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\t.foeMapping #add_your_pin_form .upload-photo-label {\r\n\t\t\t\tdisplay: inline-block;\r\n\t\t\t\twidth: 86px;\r\n\t\t\t\tmargin-right: 10px;\r\n\t\t\t}\r\n\t\t\t.smallprint {\r\n\t\t\t\tfont-size: 80%;\r\n\t\t\t\ttext-align: center;\r\n\t\t\t\tmargin: 5px 0;\r\n\t\t\t\tcolor: white;\r\n\t\t\t}\r\n\t\t\t#add_your_pin_button {\r\n\t\t\t\twidth: 170px;\r\n\t\t\t\tpadding-right: 6px;\r\n\t\t\t}\r\n\t\t\t.geolocateicon,\r\n\t\t\t#geolocate_button\t\t\t{\r\n\t\t\t\tvertical-align: middle;\r\n\t\t\t}\r\n\t\t\t#form_confirmation p {\r\n\t\t\t\tfont-weight: bold;\r\n\t\t\t}\r\n\t\t\t#form_confirmation .closeForm {\r\n\t\t\t\tbackground: #888888;\r\n\t\t\t\tfont-size: 24px;\r\n\t\t\t\twidth: auto;\r\n\t\t\t\tpadding: 7px 28px;\r\n\t\t\t}\r\n\t\t\t#form_confirmation .closeForm > p {\r\n\t\t\t\tmargin: 0;\r\n\t\t\t\tpadding: 0;\r\n\t\t\t}\r\n\t\t\t#form_confirmation > img {\r\n\t\t\t\tmargin: 10px;\r\n\t\t\t}\r\n\t\t\t#add_your_pin_form .error {\r\n\t\t\t\tbackground-color: #FFAAAA;\r\n\t\t\t}\r\n\t\t\t@media all and (max-width: 530px) {\r\n\t\t\t\t\r\n\t\t\t\t.foeMapping #legend #add_your_pin_container  {\r\n\t\t\t\t\tdisplay: none;\r\n\t\t\t\t}\r\n\r\n\t\t\t}\r\n        </style>\r\n\t\t<!--[if lte IE 8]>\r\n\t\t<style type="text/css">\r\n\t\t\t.foeMapping #add_your_pin_form button.bigOrange {\r\n\t\t\t\twidth: 152px !important;\r\n\t\t\t}\r\n\t\t</style>\r\n\t\t<![endif]-->'
        map_server_map_1.specific_html = u'<div id="add_your_pin_container">\n                <div id="add_your_pin_button" class="bigOrange divButton"><img src="/maps/static/rendered_maps/img/markers/user.png"><p>Add your own pin</p></div>\n            </div>\n\n            <form id="add_your_pin_form" action="/maps/plain/post/" method="POST" enctype="multipart/form-data" target="iframe-post-form">\n\n                <div id="close_button" class="divButton closeForm"><p>X</p></div>\n\n                <div id="form_body">\n                    <h2>Share your Bee Activity</h2>\n                    <p style="">Did you plant bee-friendly flowers? Attend a bee event? Spot a species?<br>Pin it to the map! Add a message and photo to tell us about it.</p>\n\n                    <input id="id_latitude" name="latitude" type="hidden">\n                    <input id="id_longitude" name="longitude" type="hidden">\n\n\t\t\t\t\t<div class="formimage">\n\t\t\t\t\t\t<img style="" src="/maps/static/rendered_maps/img/user-pin-large.png" />\n\t\t\t\t\t</div>\n                    <div class="textfields">\n                        <input id="id_firstname" name="firstname" type="text" placeholder="First name">\n                        <input id="id_lastname" name="lastname" type="text" placeholder="Surname">\n                        <input id="id_email" name="email" type="text" placeholder="Email">\n                        <input id="id_postcode" name="postcode" type="text" maxlength="8" placeholder="Postcode">\n                    </div>\n\n                    <textarea cols="30" id="id_description" name="description" rows="3" placeholder="Message"></textarea>\n\n                    <label class="upload-photo-label" style="color: white;"><strong>Upload photo</strong> (optional)</label>\n\t\t\t\t\t<input id="id_photo" name="photo" type="file" accept="image/*" style="">\n\t\t\t\t\t\n                    <button type="submit" class="bigOrange divButton bigOrange-submit">Submit</button>\n                    <label class="smallprint" id="dp">\n                        <input id="id_data_protection" name="data_protection_can_send" type="checkbox" checked>\n                        If you prefer we do not send you information about our activities, untick this box.\n                    </label>\n\t\t\t\t\t<p class="smallprint">Submissions are moderated. Pins display name and message/photo at the postcode provided.</p>\n                </div>\n\n                <div id="form_confirmation">\n                    <h2>Thank you for your submission</h2>\n                    <p>Your pin is currently awaiting approval.<br>You\'ll receive an email if it has been accepted.</p>\n\t\t\t\t\t<img style="" src="/maps/static/rendered_maps/img/user-pin-large.png" />\n                    <button class="bigOrange divButton closeForm" type="button" style="text-align: center;">Close</button>\n                </div>\n            </form>\n\n            <!-- Placeholder support for IE lte 9 -->\n            <!--[if lte IE 9]>\n                <script src="/maps/static/js/lib/jquery.placeholder.min.js" ></script>\n                <script>$(function() { $(\'input[placeholder], textarea[placeholder]\').placeholder(); });</script>\n            <![endif]-->'
        map_server_map_1.shareable_url_root = u'http://www.example.com/'
        map_server_map_1.published_url = u''
        map_server_map_1 = save_or_locate(map_server_map_1)

        map_server_map_1.layers.add(map_server_layer_1)
        map_server_map_1.layers.add(map_server_layer_2)
        map_server_map_1.layers.add(map_server_layer_3)
