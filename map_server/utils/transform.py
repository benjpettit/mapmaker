# coding=utf-8

from pyproj import Proj, transform

prj_wgs = Proj(init='epsg:4326')
prj_british_national_grid = Proj(init='epsg:27700')
prj_irish_grid = Proj(init='epsg:29902')
prj_irish_transverse_mercator = Proj(init='epsg:2157')


def british_grid_to_wgs(easting, northing):
    x, y = transform(prj_british_national_grid, prj_wgs, easting, northing)
    return {'lng': x, 'lat': y}


def irish_grid_to_wgs(easting, northing):
    x, y = transform(prj_irish_grid, prj_wgs, easting, northing)
    return {'lng': x, 'lat': y}



# Spire of Dublin
# Irish Grid: 315904, 234671
# WGS84: 53.349795, -6.260248
# print irish_grid_to_wgs(easting=315904.00059614395, northing=234670.99994930692)

# FoE Office
# British Grid: 532507, 182987
# WGS84: 51.530257, -0.091127
# print british_grid_to_wgs(easting=532507.363, northing=182987.60)




# def reproject_wgs_to_itm(longitude, latitude):
#     x, y = transform(prj_wgs, prj_irish_transverse_mercator, longitude, latitude)
#     return x, y
#
# print reproject_wgs_to_itm(-7.748108, 53.431628)
#
#
# def reproject_itm_to_wgs(longitude, latitude):
#     x, y = transform(prj_irish_transverse_mercator, prj_wgs, longitude, latitude)
#     return x, y
#
# print reproject_itm_to_wgs(616739.165779071, 742421.4589047553)


# def wgs_to_irish_grid(longitude, latitude):
#     x, y = transform(prj_wgs, prj_irish_grid, longitude, latitude)
#     return {'easting': x, 'northing': y}
# print wgs_to_irish_grid(latitude=53.349795563613085, longitude=-6.260248409625466)