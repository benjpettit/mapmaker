from django import forms
from django.forms import ModelForm
from map_server.models import UserSubmission


class UserSubmissionForm(ModelForm):
    # latitude = forms.CharField(widget=forms.HiddenInput, required=False)
    # longitude = forms.CharField(widget=forms.HiddenInput, required=False)
    # user_name = forms.CharField(label='Your name')
    # user_email = forms.EmailField(label='E-mail Address')

    class Meta:
        model = UserSubmission
        exclude = ('approval_status',)


class ConvertEnToLatlngForm(forms.Form):
    file_to_convert = forms.FileField()


class BulkGeocodeForm(forms.Form):
    file_to_convert = forms.FileField()
