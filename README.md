MapMaker
========
MapMaker is an open source server and web app for creating standalone publishable maps.  It uses Django on the backend and AngularJS for the map editing interface:

* Google Maps is used as the base map
* It is overlaid with toggleable layers
    - Each layer contains one or more datasets
    - Each dataset is backed by a JSON, Fusion table, or XML datasource
    - Points on a layer can be clustered and have custom icons
* Maps can be styled with extra
* Maps can accept user submissions


Code location:
https://bitbucket.org/mapmaker/mapmaker

IRC:
irc.freenode.net #foeewni

Prerequisites
-------------
* Python 2.7
* Virtualenv

#### Debian/Ubuntu

Install the required debian packages using apt-get:

`sudo apt-get install python-dev python-pip python-virtualenv libxml2-devel libxslt-devel git python-virtualenv`

#### Fedora/RedHat/CentOS
TODO

#### Mac OS X
TODO

Development Setup
-----------------

### Checkout Repo

    git clone https://bitbucket.org/mapmaker/mapmaker.git

### Activate virtualenv and Install Requirements

Before installing the required packages, we create an isolated environment with virtualenv. This prevents the installation interfering with existing installed packages.

Create the environment:

    cd checkout_dir
    virtualenv env

Activate the env and install the requirements:

    . env/bin/activate
    pip install -r requirements.txt

##### OS X Note

If pip install throws an error on OS X 10.9.2, set `ARCHFLAGS` like so:

    ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install pyproj==1.9.3 lxml==3.3.5

If the above command continues to throw an error on OS X 10.9.5, install Xcode developer tools:

    xcode-select --install

Then complete environment install as usual:

    pip install -r requirements.txt

### Database and Django App Setup

Run syncdb and migrate the map_server Django application:

    ./manage.py syncdb
    ./manage.py migrate map_server

When prompted, create a superuser with the desired username and password.

### Load Data from Fixtures

Test data is provided to give you a feel for MapMaker. Import the test data like so:

    ./manage.py runscript map_server.fixtures.maps
    ./manage.py runscript map_server.fixtures.users
    ./manage.py loaddata exclusion_boxes

### Run Development Server

To run the server using the development configuration, execute the following:

    ./manage.py runserver 127.0.0.1:8000 --settings=map_server.settings.devel

You can then access the MapMaker tool using the following URL:

    http://127.0.0.1:8000/mapmaker

The data submission moderation panel can also be accessed at:

    http://127.0.0.1:8000/

Log in using the username/password you entered when synchronising the database.

Contribute
----------
We love contributions.  If you know Python, Javascript, Django, Angular, Javascript please do contribute code:

- Check for open issues or open a fresh issue to start a discussion around a feature idea or a bug.
- Fork this repository on BitBucket.  Branch off _develop_ to make your changes.
- Write a test which shows the feature works or the bug was fixed.
- Send a pull request on BitBucket; they're reviewed every Wednesday night.  Oh, and make sure to add yourself to AUTHORS.txt.

If you're not a coder, you can still really help us.  We love contributions to documentation and artwork.

We have frequent meetups: http://www.meetup.com/London-Climate-Change-Coders/

Roadmap
-------
Moved to Bitbucket Issues: https://bitbucket.org/foeewni/mapmaker/issues?status=new&status=open

License
-------
MapMaker is licensed under the Affero GPL (AGPL) License.  See LICENSE.txt.
